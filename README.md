Development
==========

MySQL
---- 

Setup MySQL

```bash
export FLASK_APP=registration.py

cd app
flask db upgrade

mysql -u root -p -h 127.0.0.1 -P 3306
Enter password: 

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 17
Server version: 8.0.21 MySQL Community Server - GPL

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]> create database registration
MySQL [(none)]> use registration
MySQL [registration]> insert into page_views (site_name, visitor_count) VALUES ("registration.com", 0)
```

Redis
---

```bash
redis-cli --scan
redis-cli monitor
```
