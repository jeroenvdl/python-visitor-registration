from flask import Flask, render_template, flash, request, redirect, url_for, jsonify, session
from flask_session import Session
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
import redis

# App config.
DEBUG = True
SECRET_KEY = 'secret'
FLASK_APP = 'registration.py'
FLASK_ENV = 'development'
FLASK_RUN_HOST = '0.0.0.0'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:example@localhost/registration'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SESSION_TYPE = 'redis'
SESSION_REDIS = redis.from_url('redis://localhost:6379')

app = Flask(__name__)

app.config.from_object(__name__)
app.config['SECRET_KEY'] = 'secret'
app.config['FLASK_APP'] = 'registration.py'
app.config['FLASK_ENV'] = 'development'
app.config['FLASK_RUN_HOST'] = '0.0.0.0'
app.config['FLASK_DEBUG'] = 'True'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:example@localhost/registration'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_REDIS'] = 'redis://localhost:6379'

# sess = Session()
# sess.init_app(app)

cache = redis.StrictRedis(host='localhost', port=6379)

bootstrap = Bootstrap(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)



class RegistationForm(FlaskForm):
    firstname = StringField('Firstname', validators=[DataRequired()])
    lastname = StringField('Lastname', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, email):
        visitor = Registrations.query.filter_by(email=email.data).first()
        if visitor is not None:
            raise ValidationError('Please use a different email address.')

class PageViews(db.Model):
    site_id = db.Column(db.Integer, primary_key=True)
    site_name = db.Column(db.String(120))
    visitor_count = db.Column(db.Integer)

class Registrations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(64))
    lastname = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Email {}>'.format(self.email)

@app.route('/')
@app.route('/index')
def index():
    if cache.exists('visitor_count'):
        count = (cache.get('visitor_count')).decode('utf-8')
        cache.incr('visitor_count')
        pageviews = PageViews.query.get(1)
        pageviews.visitor_count += 1
        db.session.commit() 
    else:
        pageviews = PageViews.query.get(1)
        pageviews_visitor_count = pageviews.visitor_count
        cache.set('visitor_count', int(pageviews_visitor_count))
        cache.incr('visitor_count')
        #count = (session.get('visitor_count')).decode('utf-8')
        count = pageviews_visitor_count
    flash('This page has been viewed %s times.' %count)
    return render_template('index.html', title='Home', pageviews=pageviews)

@app.route('/resetcounter')
def resetcounter():
    cache.delete('visitor_count')
    pageviews = PageViews.query.get(1)
    pageviews.visitor_count = 0
    db.session.commit()
    return redirect(url_for('index'))

@app.route('/registration', methods=['GET', 'POST'])
def registration():
    form = RegistationForm()
    
    registered = (cache.get('registered')).decode('utf-8')

    if form.validate_on_submit():
        visitor = Registrations(firstname=form.firstname.data, lastname=form.lastname.data, email=form.email.data)
        db.session.add(visitor)
        db.session.commit()
        cache.set('registered', 'True')
        flash('Thank you for registering')
        return redirect(url_for('index'))

    if registered == "True":
        return redirect(url_for('show_registrations'))

    return render_template('registration.html', title='Registration', form=form)

@app.route('/new_registration', methods=['GET', 'POST'])
def new_registration():
    form = RegistationForm()
    
    if form.validate_on_submit():
        visitor = Registrations(firstname=form.firstname.data, lastname=form.lastname.data, email=form.email.data)
        db.session.add(visitor)
        db.session.commit()
        cache.set('registered', 'True')
        flash('Thank you for registering')
        return redirect(url_for('index'))

    return render_template('registration.html', title='Registration', form=form)

@app.route('/registrations/show')
def show_registrations():
    visitors = Registrations.query.all()   
    return render_template('participants.html', visitors=visitors)

if __name__ == "__main__":
    app.run()